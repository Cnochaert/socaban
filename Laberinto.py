import math

def murocercano(fila,columna):
    mf1 = math.fabs(fila - 1)  
    mf2 = math.fabs(20 - fila)  
    mc1 = math.fabs(columna - 1)
    mc2 = math.fabs(20 - columna)

    if (mf1 <= mf2):
         mfila = mf1
    else:
         mfila= mf2

    if (mc1 <= mc2):
         mcolumna = mc1
    else:
         mcolumna= mc2

    if (mfila <= mcolumna):
         return  mfila
    else:
         return mcolumna

fila1 = int(input("Ingresar el numero de la fila 1 [1 20] "))
columna1 = int(input("Ingresar el numero de la columna 1 [1 20] "))
fila2 = int(input("Ingresar el numero de la fila 2 [1 20] "))
columna2 = int(input("Ingresar el numero de la columna 2 [1 20] "))

if (murocercano(fila1,columna1) <= murocercano(fila2,columna2)):
     mascercano = murocercano(fila1,columna1)
else:
     mascercano = murocercano(fila2,columna2)

print("\nEl mas cercano esta a :",str(mascercano),"metros de \n\n")

for i in range(0,20):
    for j in range(0,20):
        
        if ( (fila1 == i and columna1 == j) or (fila2 == i and columna2 == j) ):
             print("x\t") 
        else:
             print(".\t")

 
